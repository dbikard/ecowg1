This repository contains the code used in the paper:

***On-target activity predictions enable improved CRISPR-dCas9 screens in E. coli***,

Alicia Calvo-Villamañán, Jérome Wong Ng, Arthur Chen, Lun Cui and David Bikard

The ability to block gene expression in bacteria with the catalytically inactive mutant of Cas9, 
known as dCas9, is quickly becoming a standard methodology to probe gene function, perform high-throughput screens, 
and engineer cells for desired purposes. Yet, we still lack a good understanding of the design rules that determine 
on-target activity for dCas9. Taking advantage of high-throughput screening data generated in a previous study, we 
fit a simple linear model to predict the ability of dCas9 to block the RNA polymerase based on the target sequence. 
Our model predicted guide activity with a 70% spearman correlation coefficient on an independently generated dataset. 
We further design a novel genome wide guide RNA library for E. coli MG1655, using our model to choose the best possible guides 
while avoiding guides which might be toxic to the cell or have off-target effects. This novel library exceeds the performances 
of previously published screens, and demonstrates that very good performances can be reached using only a small number of well 
designed guides. Being able to design effective, smaller libraries will help make CRISPRi screens even easier to perform 
and more cost-effective.

The analysis is divided in two jupyter notebooks:

* EcoWG1_model.ipynb
> In this notebook we train a simple linear model to predict dCas9's ability to block the RNA polymerase


* EcoWG1_library_performance.ipynb
> In this notebook we analyse the results of a screen performed with the EcoWG1 library and compare its performance to published data.

In addition a script is provided to generate novel libraries following the design rules described in the manuscript.
Usage:

> python generate_library.py --ref path_to_genbank_file --n number_guide_per_gene --output output_file 

# Live Notebook

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.pasteur.fr%2Fdbikard%2Fecowg1.git/master)