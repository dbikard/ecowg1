import pandas as pd
import numpy as np
from Bio import SeqIO
import pickle
import click
import os
from tqdm import tqdm
from compute_off_targets import offTarget, write_gene_fasta, write_promoter_fasta, write_genome_fasta, load_obj

def find_all_targets(records,strand,nb_bases_left,nb_bases_right):
    '''Extracts all target positions, including a sequence window around the target that can be used to model on-target efficiency'''

    targets=[]
    for rec in records:
        L=len(rec.seq)
        if strand=="+":
            seq=rec.seq
            def pam_pos(p):
                return (p-1)%L #we want pos to be the position of the "N" in "NGG"
        elif strand=="-":
            seq=rec.seq.reverse_complement()
            def pam_pos(p):
                return (-p)%L
                
        i=21+nb_bases_left
        
        while True:            
            p=seq.find("GG",start=i)
            left=p-21-nb_bases_left
            right=p+2+nb_bases_right

            if (p==-1)|(right>L): break # loop stops if PAM is at the very end of the sequence so the gRNA would exceed the sequence

            region=str(seq[left:right])
            guide=region[nb_bases_left:nb_bases_left+20]
            pos=pam_pos(p)
            targets.append([guide,region,rec.id,strand,pos]) #adds to the list target the gRNA seq contig, the contig number, the strand and the position
            i=p+1
    
    return targets


def add_annotations(targets,records):
    '''adds annotations to the dataframe providing the gene name (if present) or locus_tag and NaN if no gene name or locus tag is present.
    Returns a dataframe that only includes guides targeting regions marked as gene. 
    Guides target overlapping genes will be present on several rows, once with each seperate gene annotation'''
    #targets['gene']='NaN' #defines new column gene in dataframe target that contains for all rows NaN, in case a gene name of locus tag is present NaN is overwritten
    #targets['gene_ori']=None
    #targets['targets_coding_strand']=None
    #targets["second_half_gene"]=False
    genes_df=pd.DataFrame()
    
    for rec in records: 
        features=[feat for feat in rec.features if feat.type=='gene']
        for feature in tqdm(features):
            start=feature.location.start
            end=feature.location.end
            L=end-start
            locus_tag = feature.qualifiers['locus_tag'][0]
            gene_name = feature.qualifiers['gene'][0] if 'gene' in feature.qualifiers else 'NaN'
            in_gene=(targets.recid==rec.id) & (targets.pos>start) & (targets.pos<end-20)
            gene_df=targets.loc[in_gene].copy()
            gene_df["gene"]='NaN'
            gene_df["gene_ori"]=None
            gene_df["targets_coding_strand"]=None
            gene_df["second_half_gene"]=False
            gene_df[["gene","gene_ori", "locus_tag"]] = gene_name, feature.strand, locus_tag
            if feature.strand==1:
                gene_df.loc[(gene_df.pos>start+int(L/2))&(gene_df.pos<end-20),"second_half_gene"]=True
                gene_df.loc[(gene_df.strand=="+"),'targets_coding_strand']=False
                gene_df.loc[(gene_df.strand=="-"),'targets_coding_strand']=True
            elif feature.strand==-1:
                gene_df.loc[(gene_df.pos>start)&(gene_df.pos<end-int(L/2)),"second_half_gene"]=True
                gene_df.loc[(gene_df.strand=="+"),'targets_coding_strand']=True
                gene_df.loc[(gene_df.strand=="-"),'targets_coding_strand']=False
            #targets.loc[in_gene]=gene_df
            genes_df=genes_df.append(gene_df)
            
    return genes_df


bases=["A","T","G","C"]
def encode(seq):
    return np.array([[int(b==p) for b in seq] for p in bases])

def encode_seqarr(seq,r):
    '''One hot encoding of the sequence. r specifies the position range.'''
    X = np.array(
            [encode(''.join([s[i] for i in r])) for s in seq]
        )
    X = X.reshape(X.shape[0], -1)
    return X  

with open("reg_coef.pkl","rb") as handle:
    reg_coef=pickle.load(handle)
 
def predict(X):
    return np.sum(X*reg_coef,axis=1)
    
def add_on_target_predictions(targets):
    Xlin = encode_seqarr(targets["seq"],list(range(34,41))+list(range(43,59)))
    targets['score']=predict(Xlin)
    return targets
    
def add_score_quartile(targets):
    q3=np.percentile(targets["score"].values,25)
    q2=np.percentile(targets["score"].values,50)
    q1=np.percentile(targets["score"].values,75)

    score_quartile=np.ones(targets.shape[0], dtype=int)
    score_quartile[np.where(targets["score"].values < q1)]=2
    score_quartile[np.where(targets["score"].values < q2)]=3
    score_quartile[np.where(targets["score"].values < q3)]=4
    targets["score_quartile"]=score_quartile
    return targets

    
def get_off_dics(ref_file,ref_name):
    '''Computes or fetch off-target dictionnaries for this genome'''
    dic_path=os.path.join("off_dics",ref_name)
    try:
        os.makedirs(dic_path)
    except FileExistsError:
        # directory already exists
        pass
    
    required_dics=["off_minus", "off_plus", "off_11_gene", "off_9_prom_minus", "off_9_prom_plus"]
    required_dics_there=np.array([dic+".pkl" in os.listdir(dic_path) for dic in required_dics]).all()
    if required_dics_there:
        # all required dictionnaries already in path
        print("fetching off-target dictionnaries...")
        return dict([(dic,load_obj(os.path.join(dic_path,dic))) for dic in required_dics])
        
    else:
        print("generating off-target dictionnaries...")
        write_genome_fasta(ref_file) #writes at the program root for the moment
        write_gene_fasta(ref_file) #writes at the program root for the moment
        write_promoter_fasta(ref_file) #writes at the program root for the moment
        offTarget("genome.fasta",[20,12],1,dic_path,"off_plus")
        offTarget("genome.fasta",[20,12],-1,dic_path,"off_minus")
        offTarget("genes.fasta",[11],1,dic_path,"off_11_gene")
        offTarget("gene_promoters.fasta",[9],1,dic_path,"off_9_prom_plus")
        offTarget("gene_promoters.fasta",[9],-1,dic_path,"off_9_prom_minus")
        return dict([(dic,load_obj(os.path.join(dic_path,dic))) for dic in required_dics])
        
def noff(row,dic={},seedSize=12):
    ''' counts off-target positions while excluding the actual target position'''
    s=row.guide[-seedSize:]
    if s in dic[seedSize]:
        offs=dic[seedSize][s].copy()
        actual_target="{}_{}".format(row.recid,row.pos)
        if actual_target in offs: #remove the actual target position from the list of off-targets
            offs.remove(actual_target)

        return len(offs)
    else:
        return 0
        
def add_off_targets(targets,ref_file,ref_name):
    
    off_dics=get_off_dics(ref_file,ref_name)
    targets["ntargets_plus"]=targets.apply(noff,dic=off_dics["off_plus"],seedSize=20,axis=1)
    targets["ntargets_minus"]=targets.apply(noff,dic=off_dics["off_minus"],seedSize=20,axis=1)
    targets["ntargets"]=targets["ntargets_plus"]+targets["ntargets_minus"]+1 #+1 to account for the fact  that the noff function removes the actual target

    targets["noff_12_plus"]=targets.apply(noff,dic=off_dics["off_plus"],seedSize=12,axis=1)
    targets["noff_12_minus"]=targets.apply(noff,dic=off_dics["off_minus"],seedSize=12,axis=1)
    targets["noff_11_gene"]=targets.apply(noff,dic=off_dics["off_11_gene"],seedSize=11,axis=1)
    targets["noff_9_prom_plus"]=targets.apply(noff,dic=off_dics["off_9_prom_plus"],seedSize=9,axis=1)
    targets["noff_9_prom_minus"]=targets.apply(noff,dic=off_dics["off_9_prom_minus"],seedSize=9,axis=1)
    
    targets["noff_12"]=targets["noff_12_plus"]+targets["noff_12_minus"]
    targets["noff_9_prom"]=targets["noff_9_prom_plus"]+targets["noff_9_prom_minus"]
    return targets
    
badseeds=['TATAG', 'AAAGG', 'GGTTA', 'ACCCA', 'AGGGG', 'GATAT']
       
def inbadseeds(row,badseeds=badseeds):
    return str(row.guide[-5:]) in badseeds

def add_badseeds(targets):
    targets["inbadseeds"]=targets.apply(inbadseeds,badseeds=badseeds,axis=1)
    return targets

def generate_targets_df(ref_file,nb_bases_left=20,nb_bases_right=20,ref_name="default"):
    '''Generates a dataframe of all the target positions in the contigs provided in records, and computes the on-target score for each.
    The function currently exludes targets 20 bases from each sides of the contigs to enable the computation 
    of a score for dCas9.'''
    
    records=list(SeqIO.parse(ref_file,"genbank"))
    if ref_name=="default":
        ref_name=records[0].id
    
    print("finding all targets...")
    targets_plus=find_all_targets(records,"+",nb_bases_left,nb_bases_right)
    targets_minus=find_all_targets(records,"-",nb_bases_left,nb_bases_right)
    
    
    targets=pd.DataFrame(targets_plus+targets_minus,
                         columns=['guide','seq','recid','strand','pos']) #transforms the list targets in a dataframe
    
    print("adding annotations...")
    targets=add_annotations(targets,records) 
    #keep only guides in the right orientation
    #it is somewhat innefficient to find all the target positions and then only keep those in genes and in the right orientation. Consider refactoring.
    print("keeping targets on the coding strand of genes...")
    targets=targets[targets.targets_coding_strand].copy()
    print("adding on-target activity predictions...")
    targets=add_on_target_predictions(targets)
    targets=add_score_quartile(targets)
    print("adding off-targets...")
    targets=add_off_targets(targets,ref_file,ref_name)
    print("adding bad seeds...")
    targets=add_badseeds(targets)
    

    return targets
    
def rank_targets(targets):
    #sort all the guides and rank them
    targets=targets.sort_values(["locus_tag","ntargets","noff_12","noff_11_gene","noff_9_prom","inbadseeds","score_quartile", "second_half_gene", "score"], 
                           ascending=[False,  True,       True,    True,        True,         True,       True,             True,              False])

    rank=0
    g=None
    gene_ranks=[]
    for i,row in targets.iterrows():
        if g==row.locus_tag:
            rank+=1
        else:
            g=row.locus_tag
            rank=1
        
        gene_ranks.append(rank)

    targets["gene_rank"]=gene_ranks
    
    with open("all_targets.csv","w") as handle:
        targets.to_csv(handle)
        
    return targets

import time
timestr = time.strftime("%Y%m%d-%H%M%S")

@click.command()
@click.option("--ref", type=click.File("rU"), help='path to genbank file of the genome'
)
@click.option('--n', default=5, required=True, type=int, help='number of guides per gene')
@click.option('--output', default="library_{}.csv".format(timestr), help='output file path')
def generate_library(ref,n,output):
    targets=generate_targets_df(ref)
    targets=rank_targets(targets)
    library=targets[targets.gene_rank<=n]
    with open(output,"w") as handle:
        library.to_csv(handle)
    
    print("done!")
    #return library

if __name__ == '__main__':
    generate_library()
