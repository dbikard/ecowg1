import click
import numpy as np
import pickle
from collections import defaultdict
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
import os


def save_obj(obj, name):
    with open(name + '.pkl', 'wb+') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)

def load_obj(name):
    with open( name + '.pkl', 'rb') as f:
        return pickle.load(f)

def dict_grouping(s):
    d = defaultdict(list)
    for k, v in s:
        d[k].append(v)
    return d

trns=str.maketrans("ATGC","TACG")
def revcomp(seq:str):
    return seq.translate(trns)[::-1]

def compute_off_target_dic(recs,seedSizes=[8,9,10,11],search_ori=1,verbose_id=True):
    '''1:non-template strand, -1:template strand'''
    tempdic=dict([(s,[]) for s in seedSizes])
    for rec in recs:
        try:
            #consider refactoring this by spliting in more functions.
            #this will work for written gene and promoter fasta
            recid, pos, strand = rec.id.split(",")
            strand=int(strand)
            left, right = pos.split("-")
            left, right = int(left), int(right)
        except:
            #when working on the whole genome
            recid=rec.id
            left=0
            strand=1
            
        L=len(rec.seq)
        if search_ori==1:
            seq=rec.seq
            def pam_pos(p, strand):
                relpos=p+2
                if strand==1:
                    return left+relpos
                else:
                    return right-relpos-1
        else:
            seq=rec.seq.reverse_complement()  
            def pam_pos(p, strand):
                relpos=L-p-3
                if strand==1:
                    return left+relpos
                else:
                    return right-relpos-1
            
        i=0
        while True:
            p=seq.find("CC",start=i)
            if p==-1 or p>L-23: break
            for s in seedSizes:
                seed=revcomp(str(seq[p+3:p+3+s]))
                if verbose_id:
                    #required when working with genomes on several contigs
                    tempdic[s].append((seed, "{}_{}".format(recid,str(pam_pos(p,strand)))))
                else:
                    tempdic[s].append((seed, pam_pos(p,strand)))
            i=p+1
    
    resdic=dict()
    for s in seedSizes:
        resdic[s]=dict_grouping(tempdic[s])
    
    return resdic
    
def write_gene_fasta(ref_file):
    recs=SeqIO.parse(ref_file,"genbank")
    seqs=[]
    for rec in recs:
        for g in rec.features:
            if g.type in ["CDS","ncRNA","rRNA","tRNA",]:
                gene_rec= SeqRecord(g.extract(rec.seq),
                           id="{},{}-{},{}".format(rec.id,g.location.start,g.location.end,g.strand),
                           description="")
                seqs.append(gene_rec)
            
    with open("genes.fasta","w") as handle:
        SeqIO.write(seqs,handle,"fasta")
        
def write_promoter_fasta(ref_file):
    ''' Promoters are arbitratily defined as sequences 100 bases upstream to 20 downstream of all annotated gene start in the genome.'''
    recs=SeqIO.parse(ref_file,"genbank")
    seqs=[]
    for rec in recs:
        for g in rec.features:
            if g.type in ["CDS","ncRNA","rRNA","tRNA",]:
                if g.location.strand==1:
                    gauche=g.location.start-100
                    droite=g.location.start+20
                else:
                    gauche=g.location.end-20
                    droite=g.location.end+100
                prom_rec= SeqRecord(rec.seq[gauche:droite],
                           id="{},{}-{},{}".format(rec.id,gauche,droite,1),
                           description="")
                seqs.append(prom_rec)
            
    with open("gene_promoters.fasta","w") as handle:
        SeqIO.write(seqs,handle,"fasta")
        
def write_genome_fasta(ref_file):
    #converts the genbank file to fasta to feed into the offTarget functions
    recs=SeqIO.parse(ref_file,"genbank")
    with open("genome.fasta","w") as handle:
        SeqIO.write(recs,handle,"fasta")
        

def offTarget(ref,seedSizes,ori,output_dir,output):
    recs=SeqIO.parse(ref,"fasta")
    resdic=compute_off_target_dic(recs,seedSizes=seedSizes,search_ori=ori)

    save_obj(resdic,os.path.join(output_dir,output))
